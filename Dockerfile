FROM docker:19

RUN apk add --update python3 py3-pip
RUN apk add make g++ bash
RUN pip3 install awscli boto3
RUN apk add --update nodejs=14.18.1-r0
RUN apk add --update npm
RUN aws --version        
RUN docker -v
RUN node -v
RUN npm -v
RUN npm install yarn -g
RUN yarn global add aws-cdk@1.37
RUN cdk --version
