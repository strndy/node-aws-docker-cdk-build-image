# Node Aws Docker CDK build image

Ment for running in Gitlab or other CI for building and deployin' docker container or FE apps.

Contains:

- Node.JS 14.16
- Docker 19
- AWS CLI (python based)
- AWS CDK

Can be found here: https://gitlab.com/strndy/node-aws-docker-cdk-build-image/container_registry

License: [WTFPL](https://en.wikipedia.org/wiki/WTFPL).
